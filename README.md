# Notes on how I anonymize contributions in a git repository.

I use the git-filter-repo tool
  https://github.com/newren/git-filter-repo

The general process is to iteratively build an anonymization script,
by repeating the following steps:

- copy the git repository to a temporary location
- apply the current anonymization script to the temporary copy
- inspect the copy to see if it looks satisfying
- refine the script if necessary, then repeat

In the log below, I am following this process in a simple example
where I want to anonymize my development branches of a pre-existing
repository (but not the public master branch).


## Simple example

### Repository setup before anonymization

I will build a repo to anonymize by fetching a public repository, then
fetching a few branches from my non-upstream development repository,
and anonymize their commits.

```sh
# the public repository
$ git clone https://gitlab.inria.fr/fpottier/inferno.git
$ cd inferno

# my private branches
$ git fetch https://gitlab.inria.fr/gscherer/inferno.git purer-constraints \
  && git branch purer-constraints FETCH_HEAD
$ git fetch https://gitlab.inria.fr/gscherer/inferno.git generalization-trees \
  && git branch generalization-rees FETCH_HEAD
$ git fetch https://gitlab.inria.fr/gscherer/inferno.git frozen-rebase \
  && git branch frozen-rebase FETCH_HEAD

$ git branch -a
  frozen-rebase
  generalization-trees
* master
  purer-constraints
  remotes/origin/HEAD -> origin/master
  remotes/origin/master
```

### Deciding what to anonymize

I can see all the commits that are *not* in the `master` branch by
using `^master`, which denotes the set of commits not reachable from
master (see `git help revisions` to learn about how to specify
interesting sets of commits).

```sh
$ git log --all ^master
```

I see all the commits of all my branches. Now I can look at the author names that I want to anonymize.

```sh
$ git log --all ^master | grep Author | sort -u
Author: collaprog <collaprog@collaprog.inria.fr>
Author: Gabriel Scherer <gabriel.scherer@gmail.com>
Author: Olivier <olivier.martinot@inria.fr>
```

In this simple example, the full name of the co-authors to anonymize
do not occur in the text of the repository itself, only as commit
authors. (I checked with `git grep -C20 Gabriel`, which turns out some
mentions of my name that belong to the public history of the
repository.)


### Anonimization approach

To anonymize commit authors, I am going to use the `--mailmap` option of
`git filter-repo`, which lets me specify a partial mapping of author
names to transform.

Let's go back to the toplevel directory, and make a working copy of
the current repository.

```sh
cd ..
cp -r inferno inferno-tmp
```

Based on the `git log` author listing, I write a `mailmap-anon` file with the following content:

```
Anonymous Author Blue   <anon@email>   Gabriel Scherer <gabriel.scherer@gmail.com>
Anonymous Author Red    <anon@email>   Olivier <olivier.martinot@inria.fr>
Anonymous collaboration <anon@email>   collaprog <collaprog@collaprog.inria.fr>
```

I go to the working copy and run the `gilt filter-repo` command to
anonymize with this mailmap file.

```sh
$ cd inferno-tmp

# git filter-repo does not deal with "^master" very well, so instead
# I list branches names <foo> and ask it to rewrite the range master..<foo>.

$ for f in $(ls -1 .git/refs/heads/); do echo -n "master..$f "; done > refs
$ cat refs
master..frozen-rebase master..generalization-trees master..master master..purer-constraints

$ git filter-repo --refs $(cat refs) --mailmap ../mailmap-anon
Aborting: Refusing to destructively overwrite repo history since
this does not look like a fresh clone.
  (expected freshly packed repo)
Please operate on a fresh clone instead.  If you want to proceed
anyway, use --force.

# indeed, this is not a fresh clone, but a full copy of an existing repo, so it's also fine

$ git filter-repo --force --refs $(cat refs) --mailmap ../mailmap-anon
Parsed 28 commits
New history written in 0.01 seconds...
HEAD is now at e3c4607 Typo.
Completely finished after 0.02 seconds.

# now let's check that anonimization worked as intended

$ git log --all ^master | grep Author | sort -u
Author: Anonymous Author Blue <anon@email>
Author: Anonymous Author Red <anon@email>
Author: Anonymous collaboration <anon@email>

# yep, looks fine
```

(This is of course a simplified log; in practice I messed things up
several times, threw away the local copy, and refined the script.)

At this point, I strongly recommend that you write up the final
commands you used in a script that can be re-executed easily. This may
be useful for several reasons:

- If you are doing this before an anonymous submission, it's possible
  that your submission will get rejected and you have to do it again,
  submitting to another venue in a few months. By that time your
  repository will have evolved, and you will have to run the script
  again on new commits.

- In the future you will be glad to be able to come back to this
  script and be reminded of how you did things.


## Advanced `git filter-repo` options

There are many `filter-repo` options; in the general case you can
provide Python callbacks that act on the various objects in a git
repository.

Here is a fragment of a script I have used on a repository of my own
(here the entire repo was to be anonimized, not just
certain branches):

```sh
# file used to rewrite Copyright notices
cat > to-censor.txt <<EOF
literal:Nathanaël Courant==>anonymous
EOF

cd camlboot
# anonymize the main repository
git filter-repo \
   --replace-text ../to-censor.txt \
   --name-callback 'return b"anonymous"' \
   --email-callback 'return b""' \
   --message-callback 'return re.sub(b"Merge pull request (#[0-9]+) from .*", b"Merge pull request \\1 from anonymous", message)' \
   --message-callback 'return re.sub(b"gasche", b"anonymous", message)'
```

The `--replace-text` option will perform rewrites on 'literal' text
fragments or patterns. This is very convenient to anonimize copyright
notices.

The "Merge pull request" stuff is due to the fact that github merge
commits typically contain the author nicknames, so we wanted to
anonimize those as well. (I found this by just running `git log all |
grep gasche`, and similarly for other names and pseudonyms of my
co-authors.)
